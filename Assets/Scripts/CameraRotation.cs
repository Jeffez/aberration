using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;

public class CameraRotation : MonoBehaviour
{
    public InputActionReference mouseYMovement;

    private const float TURNSPEED = 15;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Turn();
    }
    void Turn()
    {
        

        float rotation = mouseYMovement.action.ReadValue<float>() * TURNSPEED;

        if(transform.eulerAngles.x < 325 && transform.eulerAngles.x > 35)
        {
            if((transform.eulerAngles.x < 180 && mouseYMovement.action.ReadValue<float>() > 0.1f) || (transform.eulerAngles.x > 180 && mouseYMovement.action.ReadValue<float>() < -0.1f))
            {
                return;
            }
        }

        transform.rotation *= Quaternion.Euler(rotation * Time.fixedDeltaTime,0, 0);
    }
}
