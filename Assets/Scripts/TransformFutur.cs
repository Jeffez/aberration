
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TransformFutur : MonoBehaviour
{
    public List<Vector3> triggerPosition;
    public GameObject defaultGameObject;
    public GameObject transformedGameObject;

    public void Update()
    {
        bool off = true;
        foreach (Vector3 vector in triggerPosition)
        {
            if (Mathf.Abs(transform.position.x - vector.x) < 0.5f && Mathf.Abs(transform.position.z - vector.z) < 0.5f)
            {
                defaultGameObject.SetActive(false);
                transformedGameObject.SetActive(true);
                off = false;
            }
        }
        if(off)
        {
            defaultGameObject.SetActive(true);
            transformedGameObject.SetActive(false);
        }
    }
} 