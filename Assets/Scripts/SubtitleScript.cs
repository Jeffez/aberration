using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SubtitleScript : MonoBehaviour
{
    public TextMeshProUGUI textMeshProUGUI;

    public Dictionary<Event, Subtitle> subtitles = new Dictionary<Event, Subtitle>();

    public List<Subtitle> subtitlesList = new List<Subtitle>(); 

    public void Start()
    {
        foreach (Subtitle _subtitle in subtitlesList)
        {
            subtitles.Add(_subtitle.triggerEvent, _subtitle);
        }
    }

    public void Notify(Event _event){
        textMeshProUGUI.text = subtitles[_event].text;
    }
}
