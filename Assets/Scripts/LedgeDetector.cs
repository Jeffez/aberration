using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class LedgeDetector : MonoBehaviour
{
    [SerializeField] private PlayerMovement player;
    [SerializeField] private float radius;

    private bool canBeDetected;

    public void Awake()
    {
        canBeDetected = true;
    }

    public void Update()
    {
        if (canBeDetected)
        {
            player.ledgeDetected = Physics.OverlapSphere(transform.position, radius).Length >= 2;
        }
        else
        {
            player.ledgeDetected = false;
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        canBeDetected = false;
    }

    private void OnTriggerExit(Collider other)
    {
        canBeDetected = true;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position, radius);
        BoxCollider bc = GetComponent<BoxCollider>();
        Gizmos.DrawCube(bc.center + transform.position,bc.size);
    }
}
