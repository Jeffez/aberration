
using System.Collections;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XR;

[RequireComponent(typeof(CharacterController))] 
public class PlayerMovement : MonoBehaviour
{
    public InputActionReference zMovement;
    public InputActionReference xMovement;
    public InputActionReference mouseMovement;
    public InputActionReference grab;


    public CharacterController characterController;

    private const float TURNSPEED = 1;
    private const float PLAYERSPEED = 5;
    private const float GRAVITY = 10;

    public bool ledgeDetected;
    public GameObject canBeGrabbed;

    private GameObject objectGrabbed;

    public SubtitleScript subtitle;

    public AudioSource footstep;
    public AudioSource pot;
    public AudioSource voice;
    public AudioClip clip;

    private bool canMove;

    private void OnEnable()
    {
        grab.action.performed += Grab;
        grab.action.canceled += UnGrab;
    }

    private void OnDisable()
    {
        grab.action.performed -= Grab;
        grab.action.canceled -= UnGrab;
    }


    // Start is called before the first frame update
    void Start()
    {
        canMove = false;
        Cursor.lockState = CursorLockMode.Locked;

        StartCoroutine("StartDialogue");
    }

    IEnumerator StartDialogue()
    {
        yield return new WaitForSecondsRealtime(0.5f);

        subtitle.Notify(Event.start);
        voice.clip = clip;
        voice.time = 0;
        voice.Play();
        yield return new WaitForSecondsRealtime(1.5f);
        canMove = true;
    }

    private void Update()
    {
        if(canBeGrabbed == null)
        {
            objectGrabbed = null;
        }
    }

    void Grab(InputAction.CallbackContext _obj)
    {
        if(canBeGrabbed != null)
        {
            objectGrabbed = canBeGrabbed;
        }
    }

    void UnGrab(InputAction.CallbackContext _obj)
    {
        if(objectGrabbed != null){
            objectGrabbed = null;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Turn();
        if (canMove)
        {
            Move();
        }
    }

    void Move()
    {
        Vector3 moveDirection = Vector3.zero;

        Vector3 forward = transform.TransformDirection(Vector3.forward);
        Vector3 right = transform.TransformDirection(Vector3.right);

        moveDirection = (forward * zMovement.action.ReadValue<float>()) + (right * xMovement.action.ReadValue<float>());

        if (ledgeDetected && zMovement.action.ReadValue<float>() > 0.1 && objectGrabbed == null)
        {
            moveDirection = (forward * zMovement.action.ReadValue<float>() * 2) + (right * xMovement.action.ReadValue<float>());
            moveDirection.y += (GRAVITY + PLAYERSPEED*2.5f);
        }
        else
        {
            moveDirection.y -= GRAVITY;
        }


        if(Mathf.Abs(moveDirection.z) > 0.1f && !footstep.isPlaying)
        {
            footstep.Play();
        }else if (Mathf.Abs(moveDirection.z) <0.1f)
        {
            footstep.Pause();
            pot.Pause();
        }

        if (objectGrabbed != null)
        {
            if (Mathf.Abs(moveDirection.z) > 0.1f && !pot.isPlaying)
            {
                pot.time = 0;
                pot.Play();
            }
            objectGrabbed.GetComponent<CharacterController>().Move(moveDirection * Time.fixedDeltaTime * PLAYERSPEED);
        }

        characterController.Move(moveDirection * Time.fixedDeltaTime * PLAYERSPEED);

        
    }

    void Turn()
    {
        transform.rotation *= Quaternion.Euler(0, mouseMovement.action.ReadValue<float>() * TURNSPEED, 0);
    }

}
