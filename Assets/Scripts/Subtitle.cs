

using System;
using UnityEngine;

[Serializable]
public class Subtitle
{
    public string text;
    public Event triggerEvent;
}

public enum Event
{
    start,
    objective,
    canGrabPot,
    futur,
    fall,
    firstAnomaly,
    secondAnomaly,
    thirdAnomaly,
    end
}