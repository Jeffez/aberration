using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class CameraFocus : MonoBehaviour
{
    public Image mainCamera;
    public Image subCamera;

    public Material presentMaterial;
    public Material futurMaterial;

    public InputActionReference switchCamera;

    private void OnEnable()
    {
        switchCamera.action.performed += Switch;
    }

    private void OnDisable()
    {
        switchCamera.action.performed -= Switch;
    }

    void Switch(InputAction.CallbackContext _obj)
    {
        if(mainCamera.material == presentMaterial)
        {
            mainCamera.material = futurMaterial;
            subCamera.material = presentMaterial;
        }else
        {
            mainCamera.material = presentMaterial;
            subCamera.material = futurMaterial;
        }
    }
}
