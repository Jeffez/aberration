using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ItemDetector : MonoBehaviour
{
    public HintScript hintScript;
    [SerializeField] private PlayerMovement player;

    public void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if(player.canBeGrabbed == null)
        {
            player.canBeGrabbed = other.gameObject;
            hintScript.Show(false);
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if(player.canBeGrabbed == null)
        {
            return;
        }
        if (player.canBeGrabbed.Equals(other.gameObject))
        {
            player.canBeGrabbed = null;
            hintScript.Hide();
        }
    }
}
