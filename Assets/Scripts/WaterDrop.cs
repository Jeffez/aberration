using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterDrop : MonoBehaviour
{
    public Vector3 endPosition;

    Vector3 startPosition;
    float startTime;
    float distance;

    const float SPEED = 200;
    


    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
        startTime = Time.time;
        distance = Vector3.Distance(startPosition, endPosition);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(Vector3.Distance(transform.position, endPosition) < 0.5)
        {
            transform.position = startPosition;
            startTime = Time.time;
        }

        float frac = ((Time.time - startTime) * SPEED * Time.fixedDeltaTime) / distance;

        transform.position = Vector3.Lerp(startPosition, endPosition, frac );
    }
}
