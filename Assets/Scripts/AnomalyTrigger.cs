using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Unity.VisualScripting.Member;

public class AnomalyTrigger : MonoBehaviour
{

    public SubtitleScript subtitle;
    public AudioSource asAnomaly;

    public AudioClip clip;
    public AudioSource sourceLine;
    public SubtitleScript subtitleScript;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Data.anomalyCount++;
            switch (Data.anomalyCount )
            {
                case 1:
                    subtitle.Notify(Event.firstAnomaly);
                    sourceLine.clip = clip;
                    sourceLine.time = 0;
                    sourceLine.Play();
                    break;
                case 2:
                    //subtitle.Notify(Event.secondAnomaly);
                    break;
                case 3:
                    //subtitle.Notify(Event.thirdAnomaly);
                    break;
                case 4:
                    subtitle.Notify(Event.end);
                    sourceLine.clip = clip;
                    sourceLine.time = 0;
                    sourceLine.Play();
                    break;

            }
            asAnomaly.time = 0;
            asAnomaly.Play();
            this.gameObject.SetActive(false);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        
    }
}
