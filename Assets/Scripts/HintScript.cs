using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HintScript : MonoBehaviour
{
    public const int MAXSHOW = 3;

    public int showHint;
    private Image image;
    public GameObject text;


    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
        Hide();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Show(bool _futur)
    {
        if (showHint < MAXSHOW && !_futur)
        {
            image.color = Color.white;
            text.GetComponent<TextMeshProUGUI>().text = "E";
            text.SetActive(true);
            showHint++;
        }

        if(_futur)
        {
            image.color = Color.white;
            text.GetComponent<TextMeshProUGUI>().text = "Q";
            text.SetActive(true);
            StartCoroutine("HideIn");
        }
    }
    public void Hide()
    {
        image.color= Color.clear;
        text.SetActive(false);
    }

    public IEnumerator HideIn()
    {
        yield return new WaitForSeconds(1.5f);
        Hide();
    }
}
