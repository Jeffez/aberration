using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScript : MonoBehaviour
{

    public AudioClip clip;
    public AudioSource source;
    public SubtitleScript subtitleScript;
    public Event eventScript;
    public HintScript hintScript;

    private bool canTrigger = true;

    private void OnTriggerEnter(Collider other)
    {
        if(canTrigger)
        {
            source.clip = clip;
            source.time = 0;
            canTrigger = false;
            subtitleScript.Notify(eventScript);
            source.Play();
            if (eventScript.Equals(Event.futur))
            {
                hintScript.Show(true);
            }
        }
    }
}
